#include "include/Variables.h"
#include "include/Video.h"
#include "include/SystemFunctions.h"

static uint16 *VideoMemory = (uint16 *)0xb8000;
static uint8 pColour = 0;
static uint8 X = 0, Y = 0;

void PrintCharacter(uint8 pCharacter)
{
	VideoMemory[0] = (pColour << 8) | pCharacter;
}

void SetColour(Colour pBackground, Colour pForeground)
{
	pColour = pForeground | (pBackground << 4);
}

void SetCursor(uint16 pX, uint16 pY)
{		//! todo: if values exceed 80 and 25
	if(pX <= 25)
		X = pX;
	if(pY <= 80)
		Y = pY;
}

void ClearScreen()
{
	for(int Iterator = 0; Iterator < 2000; Iterator++)
	{
		VideoMemory[Iterator] = (pColour << 8) | (uint8)0;
	}
	SetCursor(0, 0);
}