/* 
 * File:   variables.h
 * Author: synaps
 *
 * Created on 04 October 2014, 21:09
 */

#ifndef VARIABLES_H
#define	VARIABLES_H


typedef char int8;
typedef unsigned char uchar, uint8;

typedef short int int16;
typedef unsigned short int uint16;

typedef int int32;
typedef unsigned int uint32;

typedef long int int64;
typedef unsigned long int uint64;


#endif	/* VARIABLES_H */
