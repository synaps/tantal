/* 
 * File:   system_functions.h
 * Author: synaps
 *
 * Created on 04 October 2014, 21:05
 */

#ifndef SYSTEM_FUNCTIONS_H
#define	SYSTEM_FUNCTIONS_H

#include "Variables.h"
#include "Video.h"

// Main
extern "C" int Main();

// CoreFunctions

// Video
extern "C" void PrintCharacter(uint8 pCharacter);
extern "C" void SetColour(Colour pBackground, Colour pForeground);
extern "C" void SetCursor(uint16 pX, uint16 pY);
extern "C" void ClearScreen();

#endif	/* SYSTEM_FUNCTIONS_H */
