/* 
 * File:   language_support.h
 * Author: synaps
 *
 * Created on April 7, 2015, 12:13 AM
 */

#ifndef LANGUAGE_SUPPORT_H
#define	LANGUAGE_SUPPORT_H

#include "Variables.h"

// Virtual functions
extern "C" void __cxa_pure_virtual() {}

namespace __cxxabiv1
{
	/* guard variables */

	/* The ABI requires a 64-bit type.  */
	__extension__ typedef int __guard __attribute__((mode(__DI__)));

	extern "C" int __cxa_guard_acquire (__guard *);
	extern "C" void __cxa_guard_release (__guard *);
	extern "C" void __cxa_guard_abort (__guard *);

	extern "C" int __cxa_guard_acquire (__guard *g)
	{
		return !*(char *)(g);
	}

	extern "C" void __cxa_guard_release (__guard *g)
	{
		*(char *)g = 1;
	}

	extern "C" void __cxa_guard_abort (__guard *)
	{

	}
}

// new and delete C++ operators are being replaced with these functions
void *operator new(uint64 pSize)
{
    // malloc
}

void *operator new[](uint64 pSize)
{
    // malloc
}

void operator delete(void *p)
{
    // free
}

void operator delete[](void *p)
{
    // free
}

#endif	/* LANGUAGE_SUPPORT_H */

