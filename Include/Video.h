/* 
 * File:   Video.h
 * Author: synaps
 *
 * Created on January 28, 2015, 4:25 PM
 */

#ifndef VIDEO_H
#define	VIDEO_H

#define VideoMemoryEntries 2000

enum Colour
{
	Black = 0,
	Blue,
	Green,
	Cyan,
	Red,
	Magenta,		// 5
	Brown,
	LightGrey,
	DarkGrey,
	LightBlue,
	LightGreen,		// 10=a
	LightCyan,
	LightRed,
	LightMagenta,
	LightBrown,
	White		// 15=f
};

#endif	/* VIDEO_H */

