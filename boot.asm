[bits 32]
section .bootstrap

MultibootHeader:
	MULTIBOOT_PAGE_ALIGN	equ 1 << 0
	MULTIBOOT_MEMORY_INFO	equ 1 << 1
	MULTIBOOT_ELF			equ 1 << 16
	MULTIBOOT_HEADER_MAGIC	equ 0x1BADB002
	MULTIBOOT_HEADER_FLAGS	equ MULTIBOOT_PAGE_ALIGN | MULTIBOOT_MEMORY_INFO | MULTIBOOT_ELF
	MULTIBOOT_CHECKSUM		equ -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

	dd MULTIBOOT_HEADER_MAGIC
	dd MULTIBOOT_HEADER_FLAGS
	dd MULTIBOOT_CHECKSUM

extern code, bss, end
	dd MultibootHeader
	dd code
	dd bss
	dd end
	dd Bootstrap

[global Bootstrap]
[extern Main]
Bootstrap:
	; Getting CPUID
	; we may assume that every pc will have cpuid support, but we won't risk this time
	pushfd
	pushfd
	xor dword [esp], 0x00200000
	popfd
	pushfd
	pop eax
	xor eax, [esp]
	popfd
	and eax, 0x00200000
	jz NoCPUID

	; Configure x87 FPU only if we are going to handle its exceptions
	; if MP of CR0 is set then we have math coprocessor


	; Disabling paging if GRUB set it
    mov eax, cr0                                   ; Set the A-register to control register 0.
    and eax, 01111111111111111111111111111111b     ; Clear the PG-bit, which is bit 31.
    mov cr0, eax                                   ; Set control register 0 to the A-register.
	
CPUID:
	call Main		; Calling main kernel func
NoCPUID:
	jmp $		; Infinite loop upon the return from main


section .bss
	resb 4096
stack: